# == Class: yum_config manages the yum_config file and subsequently the yum proxy 
class yum_config(
  $proxyhost = '',
  $proxyport = '',
) {
  file { '/etc/yum.conf':
        mode    => '0644',
        owner   => 'root',
        group   => 'root',
        content => template('yum_config/yum.conf.erb'),
    }

  }
